## Selected Primary Studies (236)
[P1]	A. K. Sahu, S. Sharma, and D. Puthal, “Lightweight Multi-party Authentication and Key Agreement Protocol in IoT-based E-Healthcare Service,” ACM Trans. Multimedia Comput. Commun. Appl., vol. 17, no. 2s, pp. 1–20, Jun. 2021, doi: 10.1145/3398039.

[P2]	S. Hussain, I. Ullah, H. Khattak, M. A. Khan, C.-M. Chen, and S. Kumari, “A lightweight and provable secure identity-based generalized proxy signcryption (IBGPS) scheme for Industrial Internet of Things (IIoT),” Journal of Information Security and Applications, vol. 58, p. 102625, May 2021, doi: 10.1016/j.jisa.2020.102625.

[P3]	S. Roy, U. Rawat, and J. Karjee, “A Lightweight Cellular Automata Based Encryption Technique for IoT Applications,” IEEE Access, vol. 7, pp. 39782–39793, 2019, doi: 10.1109/ACCESS.2019.2906326.

[P4]	L. Zhou, C. Su, Z. Hu, S. Lee, and H. Seo, “Lightweight Implementations of NIST P-256 and SM2 ECC on 8-bit Resource-Constraint Embedded Device,” ACM Trans. Embed. Comput. Syst., vol. 18, no. 3, pp. 1–13, Jun. 2019, doi: 10.1145/3236010.

[P5]	S. Harb and M. Jarrah, “FPGA Implementation of the ECC Over GF(2 m ) for Small Embedded Applications,” ACM Trans. Embed. Comput. Syst., vol. 18, no. 2, pp. 1–19, Apr. 2019, doi: 10.1145/3310354.

[P6]	Y. M. Khattabi, M. M. Matalgah, and M. M. Olama, “Revisiting Lightweight Encryption for IoT Applications: Error Performance and Throughput in Wireless Fading Channels With and Without Coding,” IEEE Access, vol. 8, pp. 13429–13443, 2020, doi: 10.1109/ACCESS.2020.2966596.

[P7]	J. Sun, H. Xiong, X. Liu, Y. Zhang, X. Nie, and R. H. Deng, “Lightweight and Privacy-Aware Fine-Grained Access Control for IoT-Oriented Smart Health,” IEEE Internet Things J., vol. 7, no. 7, pp. 6566–6575, Jul. 2020, doi: 10.1109/JIOT.2020.2974257.

[P8]	L. Jiang, L. Chen, T. Giannetsos, B. Luo, K. Liang, and J. Han, “Toward Practical Privacy-Preserving Processing Over Encrypted Data in IoT: An Assistive Healthcare Use Case,” IEEE Internet Things J., vol. 6, no. 6, pp. 10177–10190, Dec. 2019, doi: 10.1109/JIOT.2019.2936532.

[P9]	A. Gupta, M. Tripathi, T. J. Shaikh, and A. Sharma, “A lightweight anonymous user authentication and key establishment scheme for wearable devices,” Computer Networks, vol. 149, pp. 29–42, Feb. 2019, doi: 10.1016/j.comnet.2018.11.021.

[P10]	J. Zhang, Y. Zhao, J. Wu, and B. Chen, “LVPDA: A Lightweight and Verifiable Privacy-Preserving Data Aggregation Scheme for Edge-Enabled IoT,” IEEE Internet Things J., vol. 7, no. 5, pp. 4016–4027, May 2020, doi: 10.1109/JIOT.2020.2978286.

[P11]	S. Liu, J. Yu, Y. Xiao, Z. Wan, S. Wang, and B. Yan, “BC-SABE: Blockchain-Aided Searchable Attribute-Based Encryption for Cloud-IoT,” IEEE Internet Things J., vol. 7, no. 9, pp. 7851–7867, Sep. 2020, doi: 10.1109/JIOT.2020.2993231.

[P12]	Z. Xu, C. Xu, W. Liang, J. Xu, and H. Chen, “A Lightweight Mutual Authentication and Key Agreement Scheme for Medical Internet of Things,” IEEE Access, vol. 7, pp. 53922–53931, 2019, doi: 10.1109/ACCESS.2019.2912870.

[P13]	M. Ali, M.-R. Sadeghi, and X. Liu, “Lightweight Revocable Hierarchical Attribute-Based Encryption for Internet of Things,” IEEE Access, vol. 8, pp. 23951–23964, 2020, doi: 10.1109/ACCESS.2020.2969957.

[P14]	S. Atiewi et al., “Scalable and Secure Big Data IoT System Based on Multifactor Authentication and Lightweight Cryptography,” IEEE Access, vol. 8, pp. 113498–113511, 2020, doi: 10.1109/ACCESS.2020.3002815.

[P15]	D. Puthal, X. Wu, N. Surya, R. Ranjan, and J. Chen, “SEEN: A Selective Encryption Method to Ensure Confidentiality for Big Sensing Data Streams,” IEEE Trans. Big Data, vol. 5, no. 3, pp. 379–392, Sep. 2019, doi: 10.1109/TBDATA.2017.2702172.

[P16]	J. Khan et al., “SMSH: Secure Surveillance Mechanism on Smart Healthcare IoT System With Probabilistic Image Encryption,” IEEE Access, vol. 8, pp. 15747–15767, 2020, doi: 10.1109/ACCESS.2020.2966656.

[P17]	N. Gupta, A. Jati, and A. Chattopadhyay, “MemEnc: A Lightweight, Low-Power, and Transparent Memory Encryption Engine for IoT,” IEEE Internet Things J., vol. 8, no. 9, pp. 7182–7191, May 2021, doi: 10.1109/JIOT.2020.3040846.

[P18]	Y. Liu, K. Wang, Y. Lin, and W. Xu, “$\mathsf{LightChain}$: A Lightweight Blockchain System for Industrial Internet of Things,” IEEE Trans. Ind. Inf., vol. 15, no. 6, pp. 3571–3581, Jun. 2019, doi: 10.1109/TII.2019.2904049.

[P19]	Y. Tian, L. Njilla, J. Yuan, and S. Yu, “Low-Latency Privacy-Preserving Outsourcing of Deep Neural Network Inference,” IEEE Internet Things J., vol. 8, no. 5, pp. 3300–3309, Mar. 2021, doi: 10.1109/JIOT.2020.3003468.

[P20]	S. Ricci, P. Dzurenda, J. Hajny, and L. Malina, “Privacy-Enhancing Group Signcryption Scheme,” IEEE Access, vol. 9, pp. 136529–136551, 2021, doi: 10.1109/ACCESS.2021.3117452.

[P21]	S. Zhang and J.-H. Lee, “A Group Signature and Authentication Scheme for Blockchain-Based Mobile-Edge Computing,” IEEE Internet Things J., vol. 7, no. 5, pp. 4557–4565, May 2020, doi: 10.1109/JIOT.2019.2960027.

[P22]	Y. Zhan, B. Wang, and R. Lu, “Cryptanalysis and Improvement of a Pairing-Free Certificateless Aggregate Signature in Healthcare Wireless Medical Sensor Networks,” IEEE Internet Things J., vol. 8, no. 7, pp. 5973–5984, Apr. 2021, doi: 10.1109/JIOT.2020.3033337.

[P23]	Z. Tang, “Secret Sharing-Based IoT Text Data Outsourcing: A Secure and Efficient Scheme,” IEEE Access, vol. 9, pp. 76908–76920, 2021, doi: 10.1109/ACCESS.2021.3075282.

[P24]	Z. Xue et al., “A Resource-Constrained and Privacy-Preserving Edge-Computing-Enabled Clinical Decision System: A Federated Reinforcement Learning Approach,” IEEE Internet Things J., vol. 8, no. 11, pp. 9122–9138, Jun. 2021, doi: 10.1109/JIOT.2021.3057653.

[P25]	M. K. Hasan et al., “Lightweight Encryption Technique to Enhance Medical Image Security on Internet of Medical Things Applications,” IEEE Access, vol. 9, pp. 47731–47742, 2021, doi: 10.1109/ACCESS.2021.3061710.

[P26]	L. Liu, H. Wang, and Y. Zhang, “Secure IoT Data Outsourcing With Aggregate Statistics and Fine-Grained Access Control,” IEEE Access, vol. 8, pp. 95057–95067, 2020, doi: 10.1109/ACCESS.2019.2961413.

[P27]	C. D. M. Pham and T. K. Dang, “A lightweight authentication protocol for D2D-enabled IoT systems with privacy,” Pervasive and Mobile Computing, vol. 74, p. 101399, Jul. 2021, doi: 10.1016/j.pmcj.2021.101399.

[P28]	Z. Huang, L. Zhang, X. Meng, and K.-K. R. Choo, “Key-Free Authentication Protocol Against Subverted Indoor Smart Devices for Smart Home,” IEEE Internet Things J., vol. 7, no. 2, pp. 1039–1047, Feb. 2020, doi: 10.1109/JIOT.2019.2948622.

[P29]	X. Guo, J. Hua, Y. Zhang, and D. Wang, “A Complexity-Reduced Block Encryption Algorithm Suitable for Internet of Things,” IEEE Access, vol. 7, pp. 54760–54769, 2019, doi: 10.1109/ACCESS.2019.2912929.

[P30]	K. Huang, “Secure Efficient Revocable Large Universe Multi-Authority Attribute-Based Encryption for Cloud-Aided IoT,” IEEE Access, vol. 9, pp. 53576–53588, 2021, doi: 10.1109/ACCESS.2021.3070907.

[P31]	J. Wei, T. V. X. Phuong, and G. Yang, “An Efficient Privacy Preserving Message Authentication Scheme for Internet-of-Things,” IEEE Trans. Ind. Inf., vol. 17, no. 1, pp. 617–626, Jan. 2021, doi: 10.1109/TII.2020.2972623.

[P32]	U. Hijawi, D. Unal, R. Hamila, A. Gastli, and O. Ellabban, “Lightweight KPABE Architecture Enabled in Mesh Networked Resource-Constrained IoT Devices,” IEEE Access, vol. 9, pp. 5640–5650, 2021, doi: 10.1109/ACCESS.2020.3048192.

[P33]	W. Xue et al., “Towards a Compressive-Sensing-Based Lightweight Encryption Scheme for the Internet of Things,” IEEE Trans. on Mobile Comput., vol. 20, no. 10, pp. 3049–3065, Oct. 2021, doi: 10.1109/TMC.2020.2992737.

[P34]	J. Yu, S. Liu, S. Wang, Y. Xiao, and B. Yan, “LH-ABSC: A Lightweight Hybrid Attribute-Based Signcryption Scheme for Cloud-Fog-Assisted IoT,” IEEE Internet Things J., vol. 7, no. 9, pp. 7949–7966, Sep. 2020, doi: 10.1109/JIOT.2020.2992288.

[P35]	J. N. Mamvong, G. L. Goteng, B. Zhou, and Y. Gao, “Efficient Security Algorithm for Power-Constrained IoT Devices,” IEEE Internet Things J., vol. 8, no. 7, pp. 5498–5509, Apr. 2021, doi: 10.1109/JIOT.2020.3033435.

[P36]	M. Letafati, A. Kuhestani, K.-K. Wong, and Md. J. Piran, “A Lightweight Secure and Resilient Transmission Scheme for the Internet of Things in the Presence of a Hostile Jammer,” IEEE Internet Things J., vol. 8, no. 6, pp. 4373–4388, Mar. 2021, doi: 10.1109/JIOT.2020.3026475.

[P37]	M. Cao et al., “Sec-D2D: A Secure and Lightweight D2D Communication System With Multiple Sensors,” IEEE Access, vol. 7, pp. 33759–33770, 2019, doi: 10.1109/ACCESS.2019.2900727.

[P38]	W. Tang, J. Ren, K. Deng, and Y. Zhang, “Secure Data Aggregation of Lightweight E-Healthcare IoT Devices With Fair Incentives,” IEEE Internet Things J., vol. 6, no. 5, pp. 8714–8726, Oct. 2019, doi: 10.1109/JIOT.2019.2923261.

[P39]	S. Sathyadevan, K. Achuthan, R. Doss, and L. Pan, “Protean Authentication Scheme – A Time-Bound Dynamic KeyGen Authentication Technique for IoT Edge Nodes in Outdoor Deployments,” IEEE Access, vol. 7, pp. 92419–92435, 2019, doi: 10.1109/ACCESS.2019.2927818.

[P40]	S. Wang, L. Yao, J. Chen, and Y. Zhang, “KS-ABESwET: A Keyword Searchable Attribute-Based Encryption Scheme With Equality Test in the Internet of Things,” IEEE Access, vol. 7, pp. 80675–80696, 2019, doi: 10.1109/ACCESS.2019.2922646.

[P41]	M. A. Rahman, M. S. Hossain, M. S. Islam, N. A. Alrajeh, and G. Muhammad, “Secure and Provenance Enhanced Internet of Health Things Framework: A Blockchain Managed Federated Learning Approach,” IEEE Access, vol. 8, pp. 205071–205087, 2020, doi: 10.1109/ACCESS.2020.3037474.

[P42]	Y. Li, Z. Dong, K. Sha, C. Jiang, J. Wan, and Y. Wang, “TMO: Time Domain Outsourcing Attribute-Based Encryption Scheme for Data Acquisition in Edge Computing,” IEEE Access, vol. 7, pp. 40240–40257, 2019, doi: 10.1109/ACCESS.2019.2907319.

[P43]	L. Zhang, X. Gao, and Y. Mu, “Secure Data Sharing With Lightweight Computation in E-Health,” IEEE Access, vol. 8, pp. 209630–209643, 2020, doi: 10.1109/ACCESS.2020.3039866.

[P44]	G. S. Gaba, G. Kumar, H. Monga, T.-H. Kim, and P. Kumar, “Robust and Lightweight Mutual Authentication Scheme in Distributed Smart Environments,” IEEE Access, vol. 8, pp. 69722–69733, 2020, doi: 10.1109/ACCESS.2020.2986480.

[P45]	J. Qian, Z. Cao, X. Dong, J. Shen, Z. Liu, and Y. Ye, “Two Secure and Efficient Lightweight Data Aggregation Schemes for Smart Grid,” IEEE Trans. Smart Grid, vol. 12, no. 3, pp. 2625–2637, May 2021, doi: 10.1109/TSG.2020.3044916.

[P46]	G. Kumar, R. Saha, M. K. Rai, R. Thomas, and T.-H. Kim, “A Lattice Signcrypted Secured Localization in Wireless Sensor Networks,” IEEE Systems Journal, vol. 14, no. 3, pp. 3949–3956, Sep. 2020, doi: 10.1109/JSYST.2019.2961476.

[P47]	R. Vinoth, L. J. Deborah, P. Vijayakumar, and N. Kumar, “Secure Multifactor Authenticated Key Agreement Scheme for Industrial IoT,” IEEE Internet Things J., vol. 8, no. 5, pp. 3801–3811, Mar. 2021, doi: 10.1109/JIOT.2020.3024703.

[P48]	E. B. Sanjuan, I. A. Cardiel, J. A. Cerrada, and C. Cerrada, “Message Queuing Telemetry Transport (MQTT) Security: A Cryptographic Smart Card Approach,” IEEE Access, vol. 8, pp. 115051–115062, 2020, doi: 10.1109/ACCESS.2020.3003998.

[P49]	E. Uchiteleva, A. R. Hussein, and A. Shami, “Lightweight Dynamic Group Rekeying for Low-Power Wireless Networks in IIoT,” IEEE Internet Things J., vol. 7, no. 6, pp. 4972–4986, Jun. 2020, doi: 10.1109/JIOT.2020.2974839.

[P50]	Y. Zhang, P. Wang, L. Fang, X. He, H. Han, and B. Chen, “Secure Transmission of Compressed Sampling Data Using Edge Clouds,” IEEE Trans. Ind. Inf., vol. 16, no. 10, pp. 6641–6651, Oct. 2020, doi: 10.1109/TII.2020.2966511.

[P51]	Z. Ning et al., “TAW: Cost-Effective Threshold Authentication With Weights for Internet of Things,” IEEE Access, vol. 7, pp. 30112–30125, 2019, doi: 10.1109/ACCESS.2019.2902226.

[P52]	M. A. Al Sibahee et al., “Lightweight Secure Message Delivery for E2E S2S Communication in the IoT-Cloud System,” IEEE Access, vol. 8, pp. 218331–218347, 2020, doi: 10.1109/ACCESS.2020.3041809.

[P53]	C. Meshram et al., “A Provably Secure Lightweight Subtree-Based Short Signature Scheme With Fuzzy User Data Sharing for Human-Centered IoT,” IEEE Access, vol. 9, pp. 3649–3659, 2021, doi: 10.1109/ACCESS.2020.3046367.

[P54]	Y. Miao, R. Deng, K.-K. R. Choo, X. Liu, J. Ning, and H. Li, “Optimized Verifiable Fine-Grained Keyword Search in Dynamic Multi-owner Settings,” IEEE Trans. Dependable and Secure Comput., pp. 1–1, 2019, doi: 10.1109/TDSC.2019.2940573.

[P55]	A. Singh, N. Chawla, J. H. Ko, M. Kar, and S. Mukhopadhyay, “Energy Efficient and Side-Channel Secure Cryptographic Hardware for IoT-Edge Nodes,” IEEE Internet Things J., vol. 6, no. 1, pp. 421–434, Feb. 2019, doi: 10.1109/JIOT.2018.2861324.

[P56]	Y. Guo, L. Li, and B. Liu, “Shadow: A Lightweight Block Cipher for IoT Nodes,” IEEE Internet Things J., vol. 8, no. 16, pp. 13014–13023, Aug. 2021, doi: 10.1109/JIOT.2021.3064203.

[P57]	P. P. Ray, N. Kumar, and D. Dash, “BLWN: Blockchain-Based Lightweight Simplified Payment Verification in IoT-Assisted e-Healthcare,” IEEE Systems Journal, vol. 15, no. 1, pp. 134–145, Mar. 2021, doi: 10.1109/JSYST.2020.2968614.

[P58]	Y.-M. Tseng, J.-L. Chen, and S.-S. Huang, “A Lightweight Leakage-Resilient Identity-Based Mutual Authentication and Key Exchange Protocol for Resource-limited Devices,” Computer Networks, vol. 196, p. 108246, Sep. 2021, doi: 10.1016/j.comnet.2021.108246.

[P59]	Y. Miao, J. Ma, X. Liu, J. Weng, H. Li, and H. Li, “Lightweight Fine-Grained Search Over Encrypted Data in Fog Computing,” IEEE Trans. Serv. Comput., vol. 12, no. 5, pp. 772–785, Sep. 2019, doi: 10.1109/TSC.2018.2823309.

[P60]	L. Li, G. Wen, Z. Wang, and Y. Yang, “Efficient and Secure Image Communication System Based on Compressed Sensing for IoT Monitoring Applications,” IEEE Trans. Multimedia, vol. 22, no. 1, pp. 82–95, Jan. 2020, doi: 10.1109/TMM.2019.2923111.

[P61]	S. Arif, M. A. Khan, S. U. Rehman, M. A. Kabir, and M. Imran, “Investigating Smart Home Security: Is Blockchain the Answer?,” IEEE Access, vol. 8, pp. 117802–117816, 2020, doi: 10.1109/ACCESS.2020.3004662.

[P62]	M. Wang, D. Xiao, and Y. Xiang, “Low-Cost and Confidentiality-Preserving Multi-Image Compressed Acquisition and Separate Reconstruction for Internet of Multimedia Things,” IEEE Internet Things J., vol. 8, no. 3, pp. 1662–1673, Feb. 2021, doi: 10.1109/JIOT.2020.3015237.

[P63]	S. Misra, A. Mukherjee, A. Roy, N. Saurabh, Y. Rahulamathavan, and M. Rajarajan, “Blockchain at the Edge: Performance of Resource-Constrained IoT Networks,” IEEE Trans. Parallel Distrib. Syst., vol. 32, no. 1, pp. 174–183, Jan. 2021, doi: 10.1109/TPDS.2020.3013892.

[P64]	B. Chen, L. Liu, and H. Ma, “HAC: Enable High Efficient Access Control for Information-Centric Internet of Things,” IEEE Internet Things J., vol. 7, no. 10, pp. 10347–10360, Oct. 2020, doi: 10.1109/JIOT.2020.2989361.

[P65]	B. Bordel, R. Alcarria, T. Robles, and M. S. Iglesias, “Data Authentication and Anonymization in IoT Scenarios and Future 5G Networks Using Chaotic Digital Watermarking,” IEEE Access, vol. 9, pp. 22378–22398, 2021, doi: 10.1109/ACCESS.2021.3055771.

[P66]	J. Zhang and G. Qu, “Physical Unclonable Function-Based Key Sharing via Machine Learning for IoT Security,” IEEE Trans. Ind. Electron., vol. 67, no. 8, pp. 7025–7033, Aug. 2020, doi: 10.1109/TIE.2019.2938462.

[P67]	M. Azeem et al., “FoG-Oriented Secure and Lightweight Data Aggregation in IoMT,” IEEE Access, vol. 9, pp. 111072–111082, 2021, doi: 10.1109/ACCESS.2021.3101668.

[P68]	Z. Jiang, W. Liu, R. Ma, S. H. Shirazi, and Y. Xie, “Lightweight Healthcare Wireless Body Area Network Scheme With Amplified Security,” IEEE Access, vol. 9, pp. 125739–125752, 2021, doi: 10.1109/ACCESS.2021.3111292.

[P69]	T. A. Alghamdi, I. Ali, N. Javaid, and M. Shafiq, “Secure Service Provisioning Scheme for Lightweight IoT Devices With a Fair Payment System and an Incentive Mechanism Based on Blockchain,” IEEE Access, vol. 8, pp. 1048–1061, 2020, doi: 10.1109/ACCESS.2019.2961612.

[P70]	R. Zhou, X. Zhang, X. Wang, G. Yang, N. Guizani, and X. Du, “Efficient and Traceable Patient Health Data Search System for Hospital Management in Smart Cities,” IEEE Internet Things J., vol. 8, no. 8, pp. 6425–6436, Apr. 2021, doi: 10.1109/JIOT.2020.3028598.

[P71]	B. Aboushosha, R. A. Ramadan, A. D. Dwivedi, A. El-Sayed, and M. M. Dessouky, “SLIM: A Lightweight Block Cipher for Internet of Health Things,” IEEE Access, vol. 8, pp. 203747–203757, 2020, doi: 10.1109/ACCESS.2020.3036589.

[P72]	S. Ebrahimi and S. Bayat-Sarmadi, “Lightweight Fuzzy Extractor Based on LPN for Device and Biometric Authentication in IoT,” IEEE Internet Things J., vol. 8, no. 13, pp. 10706–10713, Jul. 2021, doi: 10.1109/JIOT.2021.3050555.

[P73]	N. Karimian, M. Tehranipoor, D. Woodard, and D. Forte, “Unlock Your Heart: Next Generation Biometric in Resource-Constrained Healthcare Systems and IoT,” IEEE Access, vol. 7, pp. 49135–49149, 2019, doi: 10.1109/ACCESS.2019.2910753.

[P74]	X. Luo et al., “A Lightweight Privacy-Preserving Communication Protocol for Heterogeneous IoT Environment,” IEEE Access, vol. 8, pp. 67192–67204, 2020, doi: 10.1109/ACCESS.2020.2978525.

[P75]	A. D. Dwivedi, P. Morawiecki, and G. Srivastava, “Differential Cryptanalysis of Round-Reduced SPECK Suitable for Internet of Things Devices,” IEEE Access, vol. 7, pp. 16476–16486, 2019, doi: 10.1109/ACCESS.2019.2894337.

[P76]	W.-C. Wang, Y. Yona, Y. Wu, S. N. Diggavi, and P. Gupta, “SLATE: A Secure Lightweight Entity Authentication Hardware Primitive,” IEEE Trans.Inform.Forensic Secur., vol. 15, pp. 276–285, 2020, doi: 10.1109/TIFS.2019.2919393.

[P77]	W. Yang, Z. Guan, L. Wu, X. Du, and M. Guizani, “Secure Data Access Control With Fair Accountability in Smart Grid Data Sharing: An Edge Blockchain Approach,” IEEE Internet Things J., vol. 8, no. 10, pp. 8632–8643, May 2021, doi: 10.1109/JIOT.2020.3047640.

[P78]	P. Koshy, S. Babu, and B. S. Manoj, “Sliding Window Blockchain Architecture for Internet of Things,” IEEE Internet Things J., vol. 7, no. 4, pp. 3338–3348, Apr. 2020, doi: 10.1109/JIOT.2020.2967119.

[P79]	F. Farha, H. Ning, K. Ali, L. Chen, and C. Nugent, “SRAM-PUF-Based Entities Authentication Scheme for Resource-Constrained IoT Devices,” IEEE Internet Things J., vol. 8, no. 7, pp. 5904–5913, Apr. 2021, doi: 10.1109/JIOT.2020.3032518.

[P80]	S. Latif, Z. Idrees, J. Ahmad, L. Zheng, and Z. Zou, “A blockchain-based architecture for secure and trustworthy operations in the industrial Internet of Things,” Journal of Industrial Information Integration, vol. 21, p. 100190, Mar. 2021, doi: 10.1016/j.jii.2020.100190.

[P81]	K. P. Satamraju and B. Malarkodi, “A decentralized framework for device authentication and data security in the next generation internet of medical things,” Computer Communications, vol. 180, pp. 146–160, Dec. 2021, doi: 10.1016/j.comcom.2021.09.012.

[P82]	A. Cherif, M. Belkadi, and D. Sauveron, “A Lightweight and Secure Data Collection Serverless Protocol Demonstrated in an Active RFIDs Scenario,” ACM Trans. Embed. Comput. Syst., vol. 18, no. 3, pp. 1–27, May 2019, doi: 10.1145/3274667.

[P83]	H. Tian, X. Li, H. Quan, C.-C. Chang, and T. Baker, “A Lightweight Attribute-Based Access Control Scheme for Intelligent Transportation System With Full Privacy Protection,” IEEE Sensors J., vol. 21, no. 14, pp. 15793–15806, Jul. 2021, doi: 10.1109/JSEN.2020.3030688.

[P84]	D. Guo, K. Cao, J. Xiong, D. Ma, and H. Zhao, “A Lightweight Key Generation Scheme for the Internet of Things,” IEEE Internet Things J., vol. 8, no. 15, pp. 12137–12149, Aug. 2021, doi: 10.1109/JIOT.2021.3060438.

[P85]	K. Sowjanya, M. Dasgupta, and S. Ray, “A lightweight key management scheme for key-escrow-free ECC-based CP-ABE for IoT healthcare systems,” Journal of Systems Architecture, vol. 117, p. 102108, Aug. 2021, doi: 10.1016/j.sysarc.2021.102108.

[P86]	S. Sheikhpour, S.-B. Ko, and A. Mahani, “A low cost fault-attack resilient AES for IoT applications,” Microelectronics Reliability, vol. 123, p. 114202, Aug. 2021, doi: 10.1016/j.microrel.2021.114202.

[P87]	H. Du, Q. Wen, S. Zhang, and M. Gao, “A new provably secure certificateless signature scheme for Internet of Things,” Ad Hoc Networks, vol. 100, p. 102074, Apr. 2020, doi: 10.1016/j.adhoc.2020.102074.

[P88]	P. N. and K. S., “A novel security framework for densely populated Internet of Things users in pervasive service access,” Computer Communications, vol. 184, pp. 86–95, Feb. 2022, doi: 10.1016/j.comcom.2021.11.018.

[P89]	D. Xiang, X. Li, J. Gao, and X. Zhang, “A secure and efficient certificateless signature scheme for Internet of Things,” Ad Hoc Networks, vol. 124, p. 102702, Jan. 2022, doi: 10.1016/j.adhoc.2021.102702.

[P90]	M. Rana et al., “A secure and lightweight authentication scheme for next generation IoT infrastructure,” Computer Communications, vol. 165, pp. 85–96, Jan. 2021, doi: 10.1016/j.comcom.2020.11.002.

[P91]	S. A. Chaudhry et al., “An anonymous device to device access control based on secure certificate for internet of medical things systems,” Sustainable Cities and Society, vol. 75, p. 103322, Dec. 2021, doi: 10.1016/j.scs.2021.103322.

[P92]	M. Saikia, “An efficient D2D quaternion encryption system for IoT using IEEE 754 standards,” Internet of Things, vol. 11, p. 100261, Sep. 2020, doi: 10.1016/j.iot.2020.100261.

[P93]	S. Krishnan, S. Lokesh, and M. Ramya Devi, “An efficient Elman neural network classifier with cloud supported internet of things structure for health monitoring system,” Computer Networks, vol. 151, pp. 201–210, Mar. 2019, doi: 10.1016/j.comnet.2019.01.034.

[P94]	S. N. Mohanty et al., “An efficient Lightweight integrated Blockchain (ELIB) model for IoT security and privacy,” Future Generation Computer Systems, vol. 102, pp. 1027–1037, Jan. 2020, doi: 10.1016/j.future.2019.09.050.

[P95]	W. El-Shafai, A. K. Mesrega, H. E. H. Ahmed, N. A. El-Bahnasawy, and F. E. Abd El-Samie, “An efficient multimedia compression-encryption scheme using latin squares for securing Internet-of-things networks,” Journal of Information Security and Applications, vol. 64, p. 103039, Feb. 2022, doi: 10.1016/j.jisa.2021.103039.

[P96]	C. Thirumalai, S. Mohan, and G. Srivastava, “An efficient public key secure scheme for cloud and IoT security,” Computer Communications, vol. 150, pp. 634–643, Jan. 2020, doi: 10.1016/j.comcom.2019.12.015.

[P97]	P. P., M. M., S. K.P., and M. S. Sayeed, “An Enhanced Energy Efficient Lightweight Cryptography Method for various IoT devices,” ICT Express, vol. 7, no. 4, pp. 487–492, Dec. 2021, doi: 10.1016/j.icte.2021.03.007.

[P98]	M. Shuai, N. Yu, H. Wang, and L. Xiong, “Anonymous authentication scheme for smart home environment with provable security,” Computers & Security, vol. 86, pp. 132–146, Sep. 2019, doi: 10.1016/j.cose.2019.06.002.

[P99]	R. Paul, N. Ghosh, S. Sau, A. Chakrabarti, and P. Mohapatra, “Blockchain based secure smart city architecture using low resource IoTs,” Computer Networks, vol. 196, p. 108234, Sep. 2021, doi: 10.1016/j.comnet.2021.108234.

[P100]	C. Lin, D. He, X. Huang, X. Xie, and K.-K. R. Choo, “Blockchain-based system for secure outsourcing of bilinear pairings,” Information Sciences, vol. 527, pp. 590–601, Jul. 2020, doi: 10.1016/j.ins.2018.12.043.

[P101]	J. Zhang, Z. Wang, L. Shang, D. Lu, and J. Ma, “BTNC: A blockchain based trusted network connection protocol in IoT,” Journal of Parallel and Distributed Computing, vol. 143, pp. 1–16, Sep. 2020, doi: 10.1016/j.jpdc.2020.04.004.

[P102]	W. Wang, H. Huang, F. Xiao, Q. Li, L. Xue, and J. Jiang, “Computation-transferable authenticated key agreement protocol for smart healthcare,” Journal of Systems Architecture, vol. 118, p. 102215, Sep. 2021, doi: 10.1016/j.sysarc.2021.102215.

[P103]	R. O. Ogundokun, J. B. Awotunde, E. A. Adeniyi, and F. E. Ayo, “Crypto-Stegno based model for securing medical information on IOMT platform,” Multimed Tools Appl, vol. 80, no. 21–23, pp. 31705–31727, Sep. 2021, doi: 10.1007/s11042-021-11125-2.

[P104]	P. Panagiotou, N. Sklavos, E. Darra, and I. D. Zaharakis, “Cryptographic system for data applications, in the context of internet of things,” Microprocessors and Microsystems, vol. 72, p. 102921, Feb. 2020, doi: 10.1016/j.micpro.2019.102921.

[P105]	S. K. Mousavi and A. Ghaffari, “Data cryptography in the Internet of Things using the artificial bee colony algorithm in a smart irrigation system,” Journal of Information Security and Applications, vol. 61, p. 102945, Sep. 2021, doi: 10.1016/j.jisa.2021.102945.

[P106]	J. Wang, J. Chen, Y. Ren, P. K. Sharma, O. Alfarraj, and A. Tolba, “Data security storage mechanism based on blockchain industrial Internet of Things,” Computers & Industrial Engineering, vol. 164, p. 107903, Feb. 2022, doi: 10.1016/j.cie.2021.107903.

[P107]	T. Rabehaja, S. Pal, and M. Hitchens, “Design and implementation of a secure and flexible access-right delegation for resource constrained environments,” Future Generation Computer Systems, vol. 99, pp. 593–608, Oct. 2019, doi: 10.1016/j.future.2019.04.035.

[P108]	A. Lohachab and Karambir, “ECC based inter-device authentication and authorization scheme using MQTT for IoT networks,” Journal of Information Security and Applications, vol. 46, pp. 1–12, Jun. 2019, doi: 10.1016/j.jisa.2019.02.005.

[P109]	G. Margelis, X. Fafoutis, G. Oikonomou, R. Piechocki, T. Tryfonas, and P. Thomas, “Efficient DCT-based secret key generation for the Internet of Things,” Ad Hoc Networks, vol. 92, p. 101744, Sep. 2019, doi: 10.1016/j.adhoc.2018.08.014.

[P110]	K. Sowjanya, M. Dasgupta, and S. Ray, “Elliptic Curve Cryptography based authentication scheme for Internet of Medical Things,” Journal of Information Security and Applications, vol. 58, p. 102761, May 2021, doi: 10.1016/j.jisa.2021.102761.

[P111]	Y. Harbi, Z. Aliouat, A. Refoufi, S. Harous, and A. Bentaleb, “Enhanced authentication and key management scheme for securing data transmission in the internet of things,” Ad Hoc Networks, vol. 94, p. 101948, Nov. 2019, doi: 10.1016/j.adhoc.2019.101948.

[P112]	M. L. B. A. Santos, J. C. Carneiro, A. M. R. Franco, F. A. Teixeira, M. A. A. Henriques, and L. B. Oliveira, “FLAT: Federated lightweight authentication for the Internet of Things,” Ad Hoc Networks, vol. 107, p. 102253, Oct. 2020, doi: 10.1016/j.adhoc.2020.102253.

[P113]	E. Elemam, A. M. Bahaa-Eldin, N. H. Shaker, and M. Sobh, “Formal verification for a PMQTT protocol,” Egyptian Informatics Journal, vol. 21, no. 3, pp. 169–182, Sep. 2020, doi: 10.1016/j.eij.2020.01.001.

[P114]	S. Shukla, S. Thakur, S. Hussain, J. G. Breslin, and S. M. Jameel, “Identification and Authentication in Healthcare Internet-of-Things Using Integrated Fog Computing Based Blockchain Model,” Internet of Things, vol. 15, p. 100422, Sep. 2021, doi: 10.1016/j.iot.2021.100422.

[P115]	M. Šarac, N. Pavlović, N. Bacanin, F. Al-Turjman, and S. Adamović, “Increasing privacy and security by integrating a Blockchain Secure Interface into an IoT Device Security Gateway Architecture,” Energy Reports, vol. 7, pp. 8075–8082, Nov. 2021, doi: 10.1016/j.egyr.2021.07.078.

[P116]	Y. Liu and S. Zhang, “Information security and storage of Internet of Things based on block chains,” Future Generation Computer Systems, vol. 106, pp. 296–303, May 2020, doi: 10.1016/j.future.2020.01.023.

[P117]	G. Kumar, R. Saha, C. Lal, and M. Conti, “Internet-of-Forensic (IoF): A blockchain based digital forensics framework for IoT applications,” Future Generation Computer Systems, vol. 120, pp. 13–25, Jul. 2021, doi: 10.1016/j.future.2021.02.016.

[P118]	A. A. Khan, V. Kumar, M. Ahmad, and S. Rana, “LAKAF: Lightweight authentication and key agreement framework for smart grid network,” Journal of Systems Architecture, vol. 116, p. 102053, Jun. 2021, doi: 10.1016/j.sysarc.2021.102053.

[P119]	M. Wazid, A. K. Das, V. Bhat K, and A. V. Vasilakos, “LAM-CIoT: Lightweight authentication mechanism in cloud-based IoT environment,” Journal of Network and Computer Applications, vol. 150, p. 102496, Jan. 2020, doi: 10.1016/j.jnca.2019.102496.

[P120]	X. Qin, Y. Huang, Z. Yang, and X. Li, “LBAC: A lightweight blockchain-based access control scheme for the internet of things,” Information Sciences, vol. 554, pp. 222–235, Apr. 2021, doi: 10.1016/j.ins.2020.12.035.

[P121]	M. A. F. Al-Husainy, B. Al-Shargabi, and S. Aljawarneh, “Lightweight cryptography system for IoT devices using DNA,” Computers & Electrical Engineering, vol. 95, p. 107418, Oct. 2021, doi: 10.1016/j.compeleceng.2021.107418.

[P122]	A. A. Agarkar and H. Agrawal, “LRSPPP: lightweight R-LWE-based secure and privacy-preserving scheme for prosumer side network in smart grid,” Heliyon, vol. 5, no. 3, p. e01321, Mar. 2019, doi: 10.1016/j.heliyon.2019.e01321.

[P123]	R. Sarma, C. Kumar, and F. A. Barbhuiya, “MACFI: A multi-authority access control scheme with efficient ciphertext and secret key size for fog-enhanced IoT,” Journal of Systems Architecture, vol. 123, p. 102347, Feb. 2022, doi: 10.1016/j.sysarc.2021.102347.

[P124]	S. Oh, S. Park, and H. Kim, “Patterned Cipher Block for Low-Latency Secure Communication,” IEEE Access, vol. 8, pp. 44632–44642, 2020, doi: 10.1109/ACCESS.2020.2977953.

[P125]	F. Shahid, A. Khan, and G. Jeon, “Post-quantum distributed ledger for internet of things,” Computers & Electrical Engineering, vol. 83, p. 106581, May 2020, doi: 10.1016/j.compeleceng.2020.106581.

[P126]	R. Boussada, B. Hamdane, M. E. Elhdhili, and L. A. Saidane, “Privacy-preserving aware data transmission for IoT-based e-health,” Computer Networks, vol. 162, p. 106866, Oct. 2019, doi: 10.1016/j.comnet.2019.106866.

[P127]	W. Jiang, H. Li, G. Xu, M. Wen, G. Dong, and X. Lin, “PTAS: Privacy-preserving Thin-client Authentication Scheme in blockchain-based PKI,” Future Generation Computer Systems, vol. 96, pp. 185–195, Jul. 2019, doi: 10.1016/j.future.2019.01.026.

[P128]	M. Á. Prada-Delgado, I. Baturone, G. Dittmann, J. Jelitto, and A. Kind, “PUF-derived IoT identities in a zero-knowledge protocol for blockchain,” Internet of Things, vol. 9, p. 100057, Mar. 2020, doi: 10.1016/j.iot.2019.100057.

[P129]	A. A. Abd El-Latif, B. Abd-El-Atty, I. Mehmood, K. Muhammad, S. E. Venegas-Andraca, and J. Peng, “Quantum-Inspired Blockchain-Based Cybersecurity: Securing Smart Edge Utilities in IoT-Based Smart Cities,” Information Processing & Management, vol. 58, no. 4, p. 102549, Jul. 2021, doi: 10.1016/j.ipm.2021.102549.

[P130]	V. Sureshkumar, R. Amin, V. R. Vijaykumar, and S. R. Sekar, “Robust secure communication protocol for smart healthcare system with FPGA implementation,” Future Generation Computer Systems, vol. 100, pp. 938–951, Nov. 2019, doi: 10.1016/j.future.2019.05.058.

[P131]	L. Vishwakarma and D. Das, “SCAB - IoTA: Secure communication and authentication for IoT applications using blockchain,” Journal of Parallel and Distributed Computing, vol. 154, pp. 94–105, Aug. 2021, doi: 10.1016/j.jpdc.2021.04.003.

[P132]	K. Huang, X. Zhang, Y. Mu, F. Rezaeibagha, and X. Du, “Scalable and redactable blockchain with update and anonymity,” Information Sciences, vol. 546, pp. 25–41, Feb. 2021, doi: 10.1016/j.ins.2020.07.016.

[P133]	S. Jiang, D. Ye, J. Huang, Y. Shang, and Z. Zheng, “SmartSteganogaphy: Light-weight generative audio steganography model for smart embedding application,” Journal of Network and Computer Applications, vol. 165, p. 102689, Sep. 2020, doi: 10.1016/j.jnca.2020.102689.

[P134]	E. Bandara, D. Tosh, P. Foytik, S. Shetty, N. Ranasinghe, and K. De Zoysa, “Tikiri—Towards a lightweight blockchain for IoT,” Future Generation Computer Systems, vol. 119, pp. 154–165, Jun. 2021, doi: 10.1016/j.future.2021.02.006.

[P135]	L. Li, J. Liu, X. Chang, T. Liu, and J. Liu, “Toward conditionally anonymous Bitcoin transactions: A lightweight-script approach,” Information Sciences, vol. 509, pp. 290–303, Jan. 2020, doi: 10.1016/j.ins.2019.09.011.

[P136]	E. Cho et al., “TwinPeaks: An approach for certificateless public key distribution for the internet and internet of things,” Computer Networks, vol. 175, p. 107268, Jul. 2020, doi: 10.1016/j.comnet.2020.107268.

[P137]	M. Al-Asli, M. E. S. Elrabaa, and M. Abu-Amara, “FPGA-Based Symmetric Re-Encryption Scheme to Secure Data Processing for Cloud-Integrated Internet of Things,” IEEE Internet Things J., vol. 6, no. 1, pp. 446–457, Feb. 2019, doi: 10.1109/JIOT.2018.2864513.

[P138]	Z. Zhang, W. Zhang, and Z. Qin, “Fully Constant-Size CP-ABE with Privacy-Preserving Outsourced Decryption for Lightweight Devices in Cloud-Assisted IoT,” Security and Communication Networks, vol. 2021, pp. 1–16, May 2021, doi: 10.1155/2021/6676862.

[P139]	Z. Wang, “Blind Batch Encryption-Based Protocol for Secure and Privacy-Preserving Medical Services in Smart Connected Health,” IEEE Internet Things J., vol. 6, no. 6, pp. 9555–9562, Dec. 2019, doi: 10.1109/JIOT.2019.2929803.

[P140]	J. Sun, Y. Yang, Z. Liu, and Y. Qiao, “Multi-Authority Criteria-Based Encryption Scheme for IoT,” Security and Communication Networks, vol. 2021, pp. 1–15, Jul. 2021, doi: 10.1155/2021/9174630.

[P141]	S. Belguith, N. Kaaniche, M. Hammoudeh, and T. Dargahi, “PROUD: Verifiable Privacy-preserving Outsourced Attribute Based SignCryption supporting access policy Update for cloud assisted IoT applications,” Future Generation Computer Systems, vol. 111, pp. 899–918, Oct. 2020, doi: 10.1016/j.future.2019.11.012.

[P142]	M. Hossain and R. Hasan, “P-HIP: A Lightweight and Privacy-Aware Host Identity Protocol for Internet of Things,” IEEE Internet Things J., vol. 8, no. 1, pp. 555–571, Jan. 2021, doi: 10.1109/JIOT.2020.3009024.

[P143]	M. Ali, M.-R. Sadeghi, and X. Liu, “Lightweight Fine-Grained Access Control for Wireless Body Area Networks,” Sensors, vol. 20, no. 4, p. 1088, Feb. 2020, doi: 10.3390/s20041088.

[P144]	S. Awan, N. Javaid, S. Ullah, A. U. Khan, A. M. Qamar, and J.-G. Choi, “Blockchain Based Secure Routing and Trust Management in Wireless Sensor Networks,” Sensors, vol. 22, no. 2, p. 411, Jan. 2022, doi: 10.3390/s22020411.

[P145]	M. Nazari and A. Maneshi, “Chaotic Reversible Watermarking Method Based on IWT with Tamper Detection for Transferring Electronic Health Record,” Security and Communication Networks, vol. 2021, pp. 1–15, May 2021, doi: 10.1155/2021/5514944.

[P146]	W. Liang, S. Xie, J. Long, K.-C. Li, D. Zhang, and K. Li, “A double PUF-based RFID identity authentication protocol in service-centric internet of things environments,” Information Sciences, vol. 503, pp. 129–147, Nov. 2019, doi: 10.1016/j.ins.2019.06.047.

[P147]	B. Kim, J. Cho, B. Choi, J. Park, and H. Seo, “Compact Implementations of HIGHT Block Cipher on IoT Platforms,” Security and Communication Networks, vol. 2019, pp. 1–10, Dec. 2019, doi: 10.1155/2019/5323578.

[P148]	A. Alamer, B. Soh, A. H. Alahmadi, and D. E. Brumbaugh, “Prototype Device With Lightweight Protocol for Secure RFID Communication Without Reliable Connectivity,” IEEE Access, vol. 7, pp. 168337–168356, 2019, doi: 10.1109/ACCESS.2019.2954413.

[P149]	M. Vedaraj and P. Ezhumalai, “HERDE-MSNB: a predictive security architecture for IoT health cloud system,” J Ambient Intell Human Comput, vol. 12, no. 7, pp. 7333–7342, Jul. 2021, doi: 10.1007/s12652-020-02408-x.

[P150]	R. Sujarani, D. Manivannan, R. Manikandan, and B. Vidhyacharan, “Lightweight Bio-Chaos Crypt to Enhance the Security of Biometric Images in Internet of Things Applications,” Wireless Pers Commun, vol. 119, no. 3, pp. 2517–2537, Aug. 2021, doi: 10.1007/s11277-021-08342-1.

[P151]	Y. Zhang, K. Cheng, F. Khan, R. Alturki, R. Khan, and A. U. Rehman, “A mutual authentication scheme for establishing secure device-to-device communication sessions in the edge-enabled smart cities,” Journal of Information Security and Applications, vol. 58, p. 102683, May 2021, doi: 10.1016/j.jisa.2020.102683.

[P152]	Y.-S. Yang, S.-H. Lee, W.-C. Chen, C.-S. Yang, Y.-M. Huang, and T.-W. Hou, “TTAS: Trusted Token Authentication Service of Securing SCADA Network in Energy Management System for Industrial Internet of Things,” Sensors, vol. 21, no. 8, p. 2685, Apr. 2021, doi: 10.3390/s21082685.

[P153]	G. Suseela, Y. A. V. Phamila, G. Niranjana, K. Ramana, S. Singh, and B. Yoon, “Low Energy Interleaved Chaotic Secure Image Coding Scheme for Visual Sensor Networks Using Pascal’s Triangle Transform,” IEEE Access, vol. 9, pp. 134576–134592, 2021, doi: 10.1109/ACCESS.2021.3116111.

[P154]	S. Khan, A. I. Alzahrani, O. Alfarraj, N. Alalwan, and A. H. Al-Bayatti, “Resource Efficient Authentication and Session Key Establishment Procedure for Low-Resource IoT Devices,” IEEE Access, vol. 7, pp. 170615–170628, 2019, doi: 10.1109/ACCESS.2019.2955604.

[P155]	K. Wang, C.-M. Chen, Z. Tie, M. Shojafar, S. Kumar, and S. Kumari, “Forward Privacy Preservation in IoT-Enabled Healthcare Systems,” IEEE Trans. Ind. Inf., vol. 18, no. 3, pp. 1991–1999, Mar. 2022, doi: 10.1109/TII.2021.3064691.

[P156]	S. Roy, M. Shrivastava, C. V. Pandey, S. K. Nayak, and U. Rawat, “IEVCA: An efficient image encryption technique for IoT applications using 2-D Von-Neumann cellular automata,” Multimed Tools Appl, vol. 80, no. 21–23, pp. 31529–31567, Sep. 2021, doi: 10.1007/s11042-020-09880-9.

[P157]	J. Zhang, H. Liu, and L. Ni, “A Secure Energy-Saving Communication and Encrypted Storage Model Based on RC4 for EHR,” IEEE Access, vol. 8, pp. 38995–39012, 2020, doi: 10.1109/ACCESS.2020.2975208.

[P158]	D. K. Kwon, S. J. Yu, J. Y. Lee, S. H. Son, and Y. H. Park, “WSN-SLAP: Secure and Lightweight Mutual Authentication Protocol for Wireless Sensor Networks,” Sensors, vol. 21, no. 3, p. 936, Jan. 2021, doi: 10.3390/s21030936.

[P159]	S. Qi, Y. Lu, W. Wei, and X. Chen, “Efficient Data Access Control With Fine-Grained Data Protection in Cloud-Assisted IIoT,” IEEE Internet Things J., vol. 8, no. 4, pp. 2886–2899, Feb. 2021, doi: 10.1109/JIOT.2020.3020979.

[P160]	L. Li, Z. Wang, and N. Li, “Efficient Attribute-Based Encryption Outsourcing Scheme With User and Attribute Revocation for Fog-Enabled IoT,” IEEE Access, vol. 8, pp. 176738–176749, 2020, doi: 10.1109/ACCESS.2020.3025140.

[P161]	X. Lu and X. Cheng, “A Secure and Lightweight Data Sharing Scheme for Internet of Medical Things,” IEEE Access, vol. 8, pp. 5022–5030, 2020, doi: 10.1109/ACCESS.2019.2962729.

[P162]	Y. Shi, W. Wei, H. Fan, M. H. Au, and X. Luo, “A Light-Weight White-Box Encryption Scheme for Securing Distributed Embedded Devices,” IEEE Trans. Comput., vol. 68, no. 10, pp. 1411–1427, Oct. 2019, doi: 10.1109/TC.2019.2907847.

[P163]	M. I. Ahmed and G. Kannan, “Secure End to End Communications and Data Analytics in IoT Integrated Application Using IBM Watson IoT Platform,” Wireless Pers Commun, vol. 120, no. 1, pp. 153–168, Sep. 2021, doi: 10.1007/s11277-021-08439-7.

[P164]	H. Zhang et al., “Efficient and Secure Outsourcing Scheme for RSA Decryption in Internet of Things,” IEEE Internet Things J., vol. 7, no. 8, pp. 6868–6881, Aug. 2020, doi: 10.1109/JIOT.2020.2970499.

[P165]	M. Khan and S. S. Jamal, “Lightweight Chaos-Based Nonlinear Component of Block Ciphers,” Wireless Pers Commun, vol. 120, no. 4, pp. 3017–3034, Oct. 2021, doi: 10.1007/s11277-021-08597-8.

[P166]	F. Najafi, M. Kaveh, D. Martín, and M. Reza Mosavi, “Deep PUF: A Highly Reliable DRAM PUF-Based Authentication for IoT Networks Using Deep Convolutional Neural Networks,” Sensors, vol. 21, no. 6, p. 2009, Mar. 2021, doi: 10.3390/s21062009.

[P167]	K. Choudhary, G. S. Gaba, I. Butun, and P. Kumar, “MAKE-IT—A Lightweight Mutual Authentication and Key Exchange Protocol for Industrial Internet of Things,” Sensors, vol. 20, no. 18, p. 5166, Sep. 2020, doi: 10.3390/s20185166.

[P168]	T. M. Ghazal et al., “Chaos-Based Cryptographic Mechanism for Smart Healthcare IoT Systems,” Computers, Materials & Continua, vol. 71, no. 1, pp. 753–769, 2022, doi: 10.32604/cmc.2022.020432.

[P169]	A. E. Guerrero-Sanchez, E. A. Rivas-Araiza, J. L. Gonzalez-Cordoba, M. Toledano-Ayala, and A. Takacs, “Blockchain Mechanism and Symmetric Encryption in A Wireless Sensor Network,” Sensors, vol. 20, no. 10, p. 2798, May 2020, doi: 10.3390/s20102798.

[P170]	M. Goworko and J. Wytrębowicz, “A Secure Communication System for Constrained IoT Devices—Experiences and Recommendations,” Sensors, vol. 21, no. 20, p. 6906, Oct. 2021, doi: 10.3390/s21206906.

[P171]	H. Xu, Q. He, X. Li, B. Jiang, and K. Qin, “BDSS-FA: A Blockchain-Based Data Security Sharing Platform With Fine-Grained Access Control,” IEEE Access, vol. 8, pp. 87552–87561, 2020, doi: 10.1109/ACCESS.2020.2992649.

[P172]	L. Mei, C. Xu, L. Xu, X. Yu, and C. Zuo, “Verifiable Identity-Based Encryption with Keyword Search for IoT from Lattice,” Computers, Materials & Continua, vol. 68, no. 2, pp. 2299–2314, 2021, doi: 10.32604/cmc.2021.017216.

[P173]	I. Ullah, A. Alkhalifah, S. U. Rehman, N. Kumar, and M. A. Khan, “An Anonymous Certificateless Signcryption Scheme for Internet of Health Things,” IEEE Access, vol. 9, pp. 101207–101216, 2021, doi: 10.1109/ACCESS.2021.3097403.

[P174]	N. Pakniat, D. Shiraly, and Z. Eslami, “Certificateless authenticated encryption with keyword search: Enhanced security model and a concrete construction for industrial IoT,” Journal of Information Security and Applications, vol. 53, p. 102525, Aug. 2020, doi: 10.1016/j.jisa.2020.102525.

[P175]	S. Ullah, L. Marcenaro, and B. Rinner, “Secure Smart Cameras by Aggregate-Signcryption with Decryption Fairness for Multi-Receiver IoT Applications,” Sensors, vol. 19, no. 2, p. 327, Jan. 2019, doi: 10.3390/s19020327.

[P176]	D. C. G. Valadares, A. A. de Carvalho Cesar Sobrinho, A. Perkusich, and K. C. Gorgonio, “Formal Verification of a Trusted Execution Environment-Based Architecture for IoT Applications,” IEEE Internet Things J., vol. 8, no. 23, pp. 17199–17210, Dec. 2021, doi: 10.1109/JIOT.2021.3077850.

[P177]	R. McPherson and J. Irvine, “Using Smartphones to Enable Low-Cost Secure Consumer IoT Devices,” IEEE Access, vol. 8, pp. 28607–28613, 2020, doi: 10.1109/ACCESS.2020.2968627.

[P178]	N. Sasikaladevi and D. Malathi, “Privacy preserving light weight authentication protocol (LEAP) for WBAN by exploring Genus-2 HEC,” Multimed Tools Appl, vol. 78, no. 13, pp. 18037–18054, Jul. 2019, doi: 10.1007/s11042-019-7149-8.

[P179]	W. She, Z.-H. Gu, X.-K. Lyu, Q. Liu, Z. Tian, and W. Liu, “Homomorphic Consortium Blockchain for Smart Home System Sensitive Data Privacy Preserving,” IEEE Access, vol. 7, pp. 62058–62070, 2019, doi: 10.1109/ACCESS.2019.2916345.

[P180]	L. Zhou, C. Su, and K.-H. Yeh, “A Lightweight Cryptographic Protocol with Certificateless Signature for the Internet of Things,” ACM Trans. Embed. Comput. Syst., vol. 18, no. 3, pp. 1–10, Jun. 2019, doi: 10.1145/3301306.

[P181]	S. Hussain et al., “A Lightweight and Formally Secure Certificate Based Signcryption With Proxy Re-Encryption (CBSRE) for Internet of Things Enabled Smart Grid,” IEEE Access, vol. 8, pp. 93230–93248, 2020, doi: 10.1109/ACCESS.2020.2994988.

[P182]	W. Ali, I. U. Din, A. Almogren, M. Guizani, and M. Zuair, “A Lightweight Privacy-Aware IoT-Based Metering Scheme for Smart Industrial Ecosystems,” IEEE Trans. Ind. Inf., vol. 17, no. 9, pp. 6134–6143, Sep. 2021, doi: 10.1109/TII.2020.2984366.

[P183]	G. Nhu Nguyen, N. Ho Le Viet, G. Prasad Joshi, and B. Shrestha, “Intelligent Tunicate Swarm-Optimization-Algorithm-Based Lightweight Security Mechanism in Internet of Health Things,” Computers, Materials & Continua, vol. 66, no. 1, pp. 551–562, 2020, doi: 10.32604/cmc.2020.012441.

[P184]	S. S. Rani, J. A. Alzubi, S. K. Lakshmanaprabu, D. Gupta, and R. Manikandan, “Optimal users based secure data transmission on the internet of healthcare things (IoHT) with lightweight block ciphers,” Multimed Tools Appl, vol. 79, no. 47–48, pp. 35405–35424, Dec. 2020, doi: 10.1007/s11042-019-07760-5.

[P185]	Z. Pooranian, M. Shojafar, S. Garg, R. Taheri, and R. Tafazolli, “LEVER: Secure Deduplicated Cloud Storage With Encrypted Two-Party Interactions in Cyber--Physical Systems,” IEEE Trans. Ind. Inf., vol. 17, no. 8, pp. 5759–5768, Aug. 2021, doi: 10.1109/TII.2020.3021013.

[P186]	G. Xu, F. Wang, M. Zhang, and J. Peng, “Efficient and Provably Secure Anonymous User Authentication Scheme for Patient Monitoring Using Wireless Medical Sensor Networks,” IEEE Access, vol. 8, pp. 47282–47294, 2020, doi: 10.1109/ACCESS.2020.2978891.

[P187]	D. Rivera, A. Garcia, M. L. Martin-Ruiz, B. Alarcos, J. R. Velasco, and A. Gomez Oliva, “Secure Communications and Protected Data for a Internet of Things Smart Toy Platform,” IEEE Internet Things J., vol. 6, no. 2, pp. 3785–3795, Apr. 2019, doi: 10.1109/JIOT.2019.2891103.

[P188]	S. K. Mousavi, A. Ghaffari, S. Besharat, and H. Afshari, “Improving the security of internet of things using cryptographic algorithms: a case of smart irrigation systems,” J Ambient Intell Human Comput, vol. 12, no. 2, pp. 2033–2051, Feb. 2021, doi: 10.1007/s12652-020-02303-5.

[P189]	S. Taware, R. R. Chakravarthi, C. A. Palagan, K. Chandrasekaran, and N. Vadivelan, “Preserving mobile commerce IoT data using light weight SIMON block cipher cryptographic paradigm,” J Ambient Intell Human Comput, vol. 12, no. 6, pp. 6081–6089, Jun. 2021, doi: 10.1007/s12652-020-02173-x.

[P190]	A. Aziz and K. Singh, “Lightweight Security Scheme for Internet of Things,” Wireless Pers Commun, vol. 104, no. 2, pp. 577–593, Jan. 2019, doi: 10.1007/s11277-018-6035-4.

[P191]	J. Yang, W. Yang, H. Liu, and L. Zhou, “Design and Simulation of Lightweight Identity Authentication Mechanism in Body Area Network,” Security and Communication Networks, vol. 2021, pp. 1–18, Apr. 2021, doi: 10.1155/2021/5577514.

[P192]	H. Qin, H. Wang, X. Wei, L. Xue, and L. Wu, “Privacy-Preserving Wildcards Pattern Matching Protocol for IoT Applications,” IEEE Access, vol. 7, pp. 36094–36102, 2019, doi: 10.1109/ACCESS.2019.2900519.

[P193]	A. S. Reegan and V. Kabila, “Highly Secured Cluster Based WSN Using Novel FCM and Enhanced ECC-ElGamal Encryption in IoT,” Wireless Pers Commun, vol. 118, no. 2, pp. 1313–1329, May 2021, doi: 10.1007/s11277-021-08076-0.

[P194]	P. Kasyoka, M. Kimwele, and S. M. Angolo, “Towards an Efficient Certificateless Access Control Scheme for Wireless Body Area Networks,” Wireless Pers Commun, vol. 115, no. 2, pp. 1257–1275, Nov. 2020, doi: 10.1007/s11277-020-07621-7.

[P195]	H. Zhong, Y. Zhou, Q. Zhang, Y. Xu, and J. Cui, “An efficient and outsourcing-supported attribute-based access control scheme for edge-enabled smart healthcare,” Future Generation Computer Systems, vol. 115, pp. 486–496, Feb. 2021, doi: 10.1016/j.future.2020.09.021.

[P196]	Y. Miao, Q. Tong, K.-K. R. Choo, X. Liu, R. H. Deng, and H. Li, “Secure Online/Offline Data Sharing Framework for Cloud-Assisted Industrial Internet of Things,” IEEE Internet Things J., vol. 6, no. 5, pp. 8681–8691, Oct. 2019, doi: 10.1109/JIOT.2019.2923068.

[P197]	J. Yun and M. Kim, “JLVEA: Lightweight Real-Time Video Stream Encryption Algorithm for Internet of Things,” Sensors, vol. 20, no. 13, p. 3627, Jun. 2020, doi: 10.3390/s20133627.

[P198]	O. A. Khashan, “Hybrid Lightweight Proxy Re-Encryption Scheme for Secure Fog-to-Things Environment,” IEEE Access, vol. 8, pp. 66878–66887, 2020, doi: 10.1109/ACCESS.2020.2984317.

[P199]	H. Dai, P. Shi, H. Huang, R. Chen, and J. Zhao, “Towards Trustworthy IoT: A Blockchain-Edge Computing Hybrid System with Proof-of-Contribution Mechanism,” Security and Communication Networks, vol. 2021, pp. 1–13, Aug. 2021, doi: 10.1155/2021/3050953.

[P200]	X. Yang, Y. Liu, J. Wu, G. Han, Y. Liu, and X. Xi, “NOMOP-ECDSA: A Lightweight ECDSA Engine for Internet of Things,” Wireless Pers Commun, vol. 121, no. 1, pp. 171–190, Nov. 2021, doi: 10.1007/s11277-021-08629-3.

[P201]	L. Zhang, Y. Huo, Q. Ge, Y. Ma, Q. Liu, and W. Ouyang, “A Privacy Protection Scheme for IoT Big Data Based on Time and Frequency Limitation,” Wireless Communications and Mobile Computing, vol. 2021, pp. 1–10, Jul. 2021, doi: 10.1155/2021/5545648.

[P202]	A. D. Dwivedi, “BRISK: Dynamic Encryption Based Cipher for Long Term Security,” Sensors, vol. 21, no. 17, p. 5744, Aug. 2021, doi: 10.3390/s21175744.

[P203]	L. Sleem and R. Couturier, “Speck-R: An ultra light-weight cryptographic scheme for Internet of Things,” Multimed Tools Appl, vol. 80, no. 11, pp. 17067–17102, May 2021, doi: 10.1007/s11042-020-09625-8.

[P204]	H. Nasiraee and M. Ashouri-Talouki, “Anonymous decentralized attribute-based access control for cloud-assisted IoT,” Future Generation Computer Systems, vol. 110, pp. 45–56, Sep. 2020, doi: 10.1016/j.future.2020.04.011.

[P205]	M. A. Jan, F. Khan, M. Alam, and M. Usman, “A payload-based mutual authentication scheme for Internet of Things,” Future Generation Computer Systems, vol. 92, pp. 1028–1039, Mar. 2019, doi: 10.1016/j.future.2017.08.035.

[P206]	Q. Liu, B. Gong, and Z. Ning, “Research on CLPKC-IDPKC cross-domain identity authentication for IoT environment,” Computer Communications, vol. 157, pp. 410–416, May 2020, doi: 10.1016/j.comcom.2020.04.043.

[P207]	C.-C. Chang, W.-K. Lee, Y. Liu, B.-M. Goi, and R. C.-W. Phan, “Signature Gateway: Offloading Signature Generation to IoT Gateway Accelerated by GPU,” IEEE Internet Things J., vol. 6, no. 3, pp. 4448–4461, Jun. 2019, doi: 10.1109/JIOT.2018.2881425.

[P208]	A. Shifa, M. Asghar, S. Noor, N. Gohar, and M. Fleury, “Lightweight Cipher for H.264 Videos in the Internet of Multimedia Things with Encryption Space Ratio Diagnostics,” Sensors, vol. 19, no. 5, p. 1228, Mar. 2019, doi: 10.3390/s19051228.

[P209]	Y. Zhang, R. Nakanishi, M. Sasabe, and S. Kasahara, “Combining IOTA and Attribute-Based Encryption for Access Control in the Internet of Things,” Sensors, vol. 21, no. 15, p. 5053, Jul. 2021, doi: 10.3390/s21155053.

[P210]	S. Paliwal, “Hash-Based Conditional Privacy Preserving Authentication and Key Exchange Protocol Suitable for Industrial Internet of Things,” IEEE Access, vol. 7, pp. 136073–136093, 2019, doi: 10.1109/ACCESS.2019.2941701.

[P211]	S. Ahmed, S. Kumari, M. A. Saleem, K. Agarwal, K. Mahmood, and M.-H. Yang, “Anonymous Key-Agreement Protocol for V2G Environment Within Social Internet of Vehicles,” IEEE Access, vol. 8, pp. 119829–119839, 2020, doi: 10.1109/ACCESS.2020.3003298.

[P212]	P. Perazzo, F. Righetti, M. La Manna, and C. Vallati, “Performance evaluation of Attribute-Based Encryption on constrained IoT devices,” Computer Communications, vol. 170, pp. 151–163, Mar. 2021, doi: 10.1016/j.comcom.2021.02.012.

[P213]	S. Roy, M. Shrivastava, U. Rawat, C. V. Pandey, and S. K. Nayak, “IESCA: An efficient image encryption scheme using 2-D cellular automata,” Journal of Information Security and Applications, vol. 61, p. 102919, Sep. 2021, doi: 10.1016/j.jisa.2021.102919.

[P214]	A. A. Ahmed, “Lightweight Digital Certificate Management and Efficacious Symmetric Cryptographic Mechanism over Industrial Internet of Things,” Sensors, vol. 21, no. 8, p. 2810, Apr. 2021, doi: 10.3390/s21082810.

[P215]	X. Chen, Y. Liu, H.-C. Chao, and Y. Li, “Ciphertext-Policy Hierarchical Attribute-Based Encryption Against Key-Delegation Abuse for IoT-Connected Healthcare System,” IEEE Access, vol. 8, pp. 86630–86650, 2020, doi: 10.1109/ACCESS.2020.2986381.

[P216]	R. Rani et al., “Towards Green Computing Oriented Security: A Lightweight Postquantum Signature for IoE,” Sensors, vol. 21, no. 5, p. 1883, Mar. 2021, doi: 10.3390/s21051883.

[P217]	A. Khan, I. Ullah, F. Algarni, M. Naeem, M. Irfan Uddin, and M. Asghar Khan, “An Efficient Proxy Blind Signcryption Scheme for IoT,” Computers, Materials & Continua, vol. 70, no. 3, pp. 4293–4306, 2022, doi: 10.32604/cmc.2022.017318.

[P218]	M. Masud, M. Alazab, K. Choudhary, and G. S. Gaba, “3P-SAKE: Privacy-preserving and physically secured authenticated key establishment protocol for wireless industrial networks,” Computer Communications, vol. 175, pp. 82–90, Jul. 2021, doi: 10.1016/j.comcom.2021.04.021.

[P219]	L. Zhao, “Privacy-Preserving Distributed Analytics in Fog-Enabled IoT Systems,” Sensors, vol. 20, no. 21, p. 6153, Oct. 2020, doi: 10.3390/s20216153.

[P220]	R. Zhou, X. Zhang, X. Wang, G. Yang, H. Wang, and Y. Wu, “Privacy-preserving data search with fine-grained dynamic search right management in fog-assisted Internet of Things,” Information Sciences, vol. 491, pp. 251–264, Jul. 2019, doi: 10.1016/j.ins.2019.04.003.

[P221]	Y. Zhao, L. T. Yang, and J. Sun, “Privacy-Preserving Tensor-Based Multiple Clusterings on Cloud for Industrial IoT,” IEEE Trans. Ind. Inf., vol. 15, no. 4, pp. 2372–2381, Apr. 2019, doi: 10.1109/TII.2018.2871174.

[P222]	M. Morales-Sandoval, R. De-La-Parra-Aguirre, H. Galeana-Zapien, and A. Galaviz-Mosqueda, “A Three-Tier Approach for Lightweight Data Security of Body Area Networks in E-Health Applications,” IEEE Access, vol. 9, pp. 146350–146365, 2021, doi: 10.1109/ACCESS.2021.3123456.

[P223]	M. Shen, X. Tang, L. Zhu, X. Du, and M. Guizani, “Privacy-Preserving Support Vector Machine Training Over Blockchain-Based Encrypted IoT Data in Smart Cities,” IEEE Internet Things J., vol. 6, no. 5, pp. 7702–7712, Oct. 2019, doi: 10.1109/JIOT.2019.2901840.

[P224]	N. Alassaf, A. Gutub, S. A. Parah, and M. Al Ghamdi, “Enhancing speed of SIMON: A light-weight-cryptographic algorithm for IoT applications,” Multimed Tools Appl, vol. 78, no. 23, pp. 32633–32657, Dec. 2019, doi: 10.1007/s11042-018-6801-z.

[P225]	Aroosa, S. S. Ullah, S. Hussain, R. Alroobaea, and I. Ali, “Securing NDN-Based Internet of Health Things through Cost-Effective Signcryption Scheme,” Wireless Communications and Mobile Computing, vol. 2021, pp. 1–13, Apr. 2021, doi: 10.1155/2021/5569365.

[P226]	T. Mahmood Butt, R. Riaz, C. Chakraborty, S. Shahla Rizvi, and A. Paul, “Cogent and Energy Efficient Authentication Protocol for WSN in IoT,” Computers, Materials & Continua, vol. 68, no. 2, pp. 1877–1898, 2021, doi: 10.32604/cmc.2021.014966.

[P227]	S. Raza and R. Mar Magnusson, “TinyIKE: Lightweight IKEv2 for Internet of Things,” IEEE Internet Things J., vol. 6, no. 1, pp. 856–866, Feb. 2019, doi: 10.1109/JIOT.2018.2862942.

[P228]	F. Deng, Y. Wang, L. Peng, M. Lai, and J. Geng, “Revocable Cloud-Assisted Attribute-Based Signcryption in Personal Health System,” IEEE Access, vol. 7, pp. 120950–120960, 2019, doi: 10.1109/ACCESS.2019.2933636.

[P229]	S. Gabsi, Y. Kortli, V. Beroulle, Y. Kieffer, A. Alasiry, and B. Hamdi, “Novel ECC-Based RFID Mutual Authentication Protocol for Emerging IoT Applications,” IEEE Access, vol. 9, pp. 130895–130913, 2021, doi: 10.1109/ACCESS.2021.3112554.

[P230]	S. Roy, U. Rawat, H. A. Sareen, and S. K. Nayak, “IECA: an efficient IoT friendly image encryption technique using programmable cellular automata,” J Ambient Intell Human Comput, vol. 11, no. 11, pp. 5083–5102, Nov. 2020, doi: 10.1007/s12652-020-01813-6.

[P231]	Q. Jiang, X. Huang, N. Zhang, K. Zhang, X. Ma, and J. Ma, “Shake to Communicate: Secure Handshake Acceleration-Based Pairing Mechanism for Wrist Worn Devices,” IEEE Internet Things J., vol. 6, no. 3, pp. 5618–5630, Jun. 2019, doi: 10.1109/JIOT.2019.2904177.

[P232]	S. Abed, R. Jaffal, B. Mohd, and M. Alshayeji, “FPGA Modeling and Optimization of a SIMON Lightweight Block Cipher,” Sensors, vol. 19, no. 4, p. 913, Feb. 2019, doi: 10.3390/s19040913.

[P233]	M. Mandal and R. Dutta, “Identity-based outsider anonymous cloud data outsourcing with simultaneous individual transmission for IoT environment,” Journal of Information Security and Applications, vol. 60, p. 102870, Aug. 2021, doi: 10.1016/j.jisa.2021.102870.

[P234]	K. Park et al., “LAKS-NVT: Provably Secure and Lightweight Authentication and Key Agreement Scheme Without Verification Table in Medical Internet of Things,” IEEE Access, vol. 8, pp. 119387–119404, 2020, doi: 10.1109/ACCESS.2020.3005592.

[P235]	M. Abu-Tair et al., “Towards Secure and Privacy-Preserving IoT Enabled Smart Home: Architecture and Experimental Study,” Sensors, vol. 20, no. 21, p. 6131, Oct. 2020, doi: 10.3390/s20216131.

[P236]	S. Zeadally, A. K. Das, and N. Sklavos, “Cryptographic technologies and protocol standards for Internet of Things,” Internet of Things, vol. 14, p. 100075, Jun. 2021, doi: 10.1016/j.iot.2019.100075.
